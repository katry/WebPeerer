class RTCSchema {
	#me = null;
	#graph = {};
	#tree = {};

	constructor(me, config) {
		this.#me = me;
		this.#graph[me.id] = me;
		this.#config = config;
	}

	init(nodes) {
		this.#initGraph(nodes);
		this.#initTree();
	}

	find(target, excludes=[]) {
		if(this.#me.peers[target])
			return target;
		let node = this.#tree[target];
		let path = [];
		while(node?.parent) {
			path.unshift(node.id);
			node = node.parent;
		}
		if(excludes.includes(path[0]))
			1; // graph search
		return path;
	}

	addConnection(data) {
		for(let index in data)
			if(!this.#graph[data[index]])
				this.#graph[data[index]] = new RTCPeer(data[index], this.#config.rtc);
		if(!this.#graph[data[0]].peers[data[1]] && ! this.#graph[data[1]].peers[data[0]])
		{
			this.#graph[data[0]].peers[data[1]] = this.#graph[data[1]];
			this.#graph[data[1]].peers[data[0]] = this.#graph[data[0]];
			this.#checkTree(data[1], data[0], false);
		}
	}

	removeConnection(data) {
		if(this.#graph[data[0]] && this.#graph[data[1]] && this.#graph[data[0]].peers[data[1]] && this.#graph[data[1]].peers[data[0]]) {
			delete this.#graph[data[0]].peers[data[1]];
			delete this.#graph[data[1]].peers[data[0]];
			this.#checkTree(data[0], data[1]);
			if(!Object.keys(this.#graph[data[0]].peers).length) {
				delete this.#graph[data[0]];
			}
			if(!Object.keys(this.#graph[data[1]].peers).length) {
				delete this.#graph[data[1]];
			}
		}
	}

	#initGraph(nodes) {
		for(let key in nodes) {
			if(!this.#graph[key])
				this.#graph[key] = new RTCPeer(key, this.#config.rtc);
			for(let i in nodes[key]) {
				let connection = nodes[key][i];
				if(!this.#graph[connection])
					this.#graph[connection] = new RTCPeer(connection, this.#config.rtc);
				this.#graph[connection].peers[key] = this.#graph[key];
				if(!this.#graph[key].peers[connection])
					this.#graph[key].peers[connection] = this.#graph[connection];
			}
		}
	}

	#initTree() {}

	#checkTree() {}
}

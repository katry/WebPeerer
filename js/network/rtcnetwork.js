class RTCNetwork {
	static #config;
	static #listeners;
	static #schema;
	static #connections = {};
	static #me = null;

	static connect(config={}) {
		RTCNetwork.#config = config;
		RTCNetwork.#listeners = {}
		RTCNetwork.#me = RTCPeer(config.me || null, config.rtc);
		RTCNetwork.#schema = new RTCSchema(me, config);
		Signalizer.init(config, RTCNetwork.#schema);
		Signalizer.addEventListener("init", (id) => { RTCNetwork.#me.id = id; });
		Signalizer.addEventListener("id", (id) => { RTCNetwork.createNode(id); });
		if(config.backups)
			RTCNetwork.#load(config.backups);
		Signalizer.connect();
	}

	static disconnect() {
		for(let key in RTCNetwork.#connections) {
			RTCNetwork.#connections[key].disconnect();
		}
		RTCNetwork.#nodes = {};
		RTCNetwork.#connections = {};
		RTCNetwork.#me = null;
	}

	static send(action, data, target, sender=null, path=[], id=null) {
		if(target === RTCNetwork.#me.id)
			return
		path.push(RTCNetwork.#me.id);
		let metadata = {
			id: id || RTCNetwork.#generateId(),
			action: action,
			path: path,
			sender: sender || RTCNetwork.#me.id
		};
		let next = 1; //choose next node;
		RTCNetwork.#connections[next].send(data, metadata);
	}

	static broadcast(action, data, excludes=[], sender=null, id=null) {
		excludes.push(RTCNetwork.#me.id);
		let metadata = {
			id: id || RTCNetwork.#generateId(),
			action: action,
			excludes: excludes,
			sender: sender || RTCNetwork.#me.id
		};
		for(let key in RTCNetwork.#connections) {
			if(excludes.includes(key))
				continue;
			RTCNetwork.#connections[key].send(data, metadata);
		}
	}

	static addEventListener(type, listener) {
		if(!RTCNetwork.#listeners[type])
			throw new ReferenceError("Failed to execute 'addEventListener' on 'RTCPeer' - type is not implemented event")
		if(typeof(listener) !== "function")
			throw new ReferenceError("Failed to execute 'addEventListener' on 'RTCPeer' - listener is not of type 'Function'")
		RTCNetwork.#listeners[type].push(listener);
	}

	static removeEventListener(type, listener) {
		if(!RTCNetwork.#listeners[type])
			throw new ReferenceError("Failed to execute 'removeEventListener' on 'RTCPeer' - type is not implemented event")
		let index = RTCNetwork.#listeners[type].indexOf(listener);
		while(index !== -1) {
			RTCNetwork.#listeners[type].splice(index, 1);
			index = RTCNetwork.#listeners[type].indexOf(listener);
		}
	}

	static #generateId() {
		let now = new Date();
		let utc = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
		return RTCNetwork.#me.id + "#" + (utc/1000).toString(36) + "#" + Math.random().toString(36).substring(7);
	}

	static #load(backups) {
		let id = Object.keys(backups)[0];
		if(!id) return;
		let node = RTCNetwork.createNode(id, backups);
		node.accept(backups[id].offer, backups[id].candidates);
		delete backups[id];
	}

	static createNode(id, backups=null) {
		let node = RTCPeer(id, RTCNetwork.#config.rtc);
		node.addEventListener("readystatechange", (peer) => {
			switch(peer.state) {
				case "open":
					if(backups) {
						RTCNetwork.#schema.addNode(peer);
						backups = null;
					}
					RTCNetwork.connections[peer.key] = peer;
				break;
				case "closed":
				case "closing":
					if(backups)
						RTCNetwork.#load(backups);
					else if(RTCNetwork.#me)
						Signalizer.connect();
					if(!Object.keys(peer.peers).length)
						delete RTCNetwork.nodes[peer.key];
				break;
			}
		});
		node.addEventListener("message", (message) => {
			// timestamp control
			// signature control
			if(message.target && message.target !== RTCNetwork.#me.id) {
				RTCNetwork.send(message.action, message.data, message.target, message.sender, message.path, message.id);
				return;
			}
			let skipBroadcast = false;
			switch(message.action) {
				case "network:init":
					RTCNetwork.#schema.init(message.data);
				break;
				case "network:add-connection":
					skipBroadcast = !RTCNetwork.#schema.addConnection(message.data);
				break;
				case "network:remove-connection":
					skipBroadcast = !RTCNetwork.#schema.removeConnection(message.data);
				break;
			}
			if(message.action.indexOf("network:") !== 0)
				1; // call network onmessage listeners
			if(!skipBroadcast && message.excludes) {
				RTCNetwork.broadcast(message.action, message.data, message.excludes, message.sender, message.id);
			}
		});
	}
}

class RTCSignalizer {
  static #config;
  static #listeners;
  static #sockets = [];
	static #connected = false;

  init(config) {
		RTCSignalizer.#config = config;
    RTCSignalizer.#listeners = {"init": [], "id": []};
	}

  connect() {
		if(this.#connected)
			return;
		this.#connected = true;
	}
	disconnect() {
		if(!this.#connected)
			return;
		this.#connected = false
	}
  send() {}
}

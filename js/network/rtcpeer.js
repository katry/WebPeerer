class RTCPeer {
	#id;
	#peers;
	#config;
	#connection = null;
	#channel = null;
	#listeners;
	#description;
	#candidates = [];

	constructor(id, config, peers={}) {
		this.#id = id;
		this.#peers = peers;signalizer
		this.#config = config;
		this.#listeners = {readystatechange: [], message: [], error: [], id: []};
	}

	#initializeConnection() {
		this.#connection = new RTCPeerConnection(this.#config);
		this.#connection.addEventListener("iceconnectionstatechange", e => {
			for(let i in this.#listeners["readystatechange"])
				this.#listeners["readystatechange"][i](e)
		});
		this.#connection.addEventListener("icecandidate", (e) => {
			if (e.candidate)
				this.#candidates.push(e.candidate);
		});
		this.#connection.addEventListener("icegatheringstatechange", () => {
			if(this.#connection.iceGatheringState == "complete" && this.#description)
				RTCSignalizer.send(this.#description, this.#candidates, this);
		});
	}

	#initializeChannel() {
		this.#channel.addEventListener("message", (e) => {
			let data;
			try { data = JSON.parse(e.data); }
			catch(e) { data = e.data; }
			for(let i in this.#listeners["message"])
				this.#listeners["message"][i](data, this);
		});
		this.#channel.addEventListener("open", () => {
			for(let i in this.#listeners["readystatechange"])
				this.#listeners["readystatechange"][i](this);
		});
		this.#channel.addEventListener("close", () => {
			for(let i in this.#listeners["readystatechange"])
				this.#listeners["readystatechange"][i](this);
		});
		this.#channel.addEventListener("closing", () => {
			for(let i in this.#listeners["readystatechange"])
				this.#listeners["readystatechange"][i](this);
		});
		this.#channel.addEventListener("error", (e) => {
			for(let i in this.#listeners["error"])
				this.#listeners["error"][i](e, this);
		});
	}

	send(data, metadata={}) {
		if(this.state !== "open")
			throw new Error("Failed to execute 'addEventListener' on 'RTCPeer' - peer not connected");
		if(typeof(metadata) != "object")
			throw new Error("Failed to execute 'addEventListener' on 'RTCPeer' - metadata is not of type Object");
		metadata.data = data;
		if(!metadata.target)
			metadata.target = this.#id;
		this.#channel.send(JSON.stringify(metadata));
	}

	disconnect() {
		if(!recieved) {
			try {
				this.#channel.close();
				this.#connection.close();
			}
			catch(e) {}
			this.#connection = null;
			this.#channel = null;
		}
	}

	connect() {
		this.#initializeConnection();
		this.#channel = this.#connection.createDataChannel("data");
		this.#initializeChannel();
		this.#connection.createOffer().then(offer => {
			this.#connection.setLocalDescription(offer).then(() => {
				if(this.#connection.iceGatheringState === "complete")
					RTCSignalizer.send(offer, this.#candidates, this);
				else this.#description = offer;
			});
		});
	}

	accept(offer, candidates) {
		this.#initializeConnection();
		this.#connection.addEventListener("datachannel", e => {
			this.#channel = e.channel;
			this.#initializeChannel();
		});

		this.#connection.setRemoteDescription(new RTCSessionDescription(offer)).then(() => {
			this.#connection.createAnswer().then(answer => {
				this.#connection.setLocalDescription(answer).then(async () => {
					await this.addCandidates(candidates);
					if(this.#connection.iceGatheringState == "complete")
						RTCSignalizer.send(answer, this.#candidates, this);
					else this.#description = answer;
				});
			});
		});
	}

	confirm(answer, candidates) {
		if(Object.keys(!this.#peers).length) {
			this.#id = data.sender;
			for(let i in this.#listeners["id"])
				this.#listeners["id"][i](answer, candidates, this);
		}
		this.#connection.setRemoteDescription(new RTCSessionDescription(answer)).then(() => {
			this.addCandidates(candidates);
		});
	}

	addEventListener(type, listener) {
		if(!this.#listeners[type])
			throw new ReferenceError("Failed to execute 'addEventListener' on 'RTCPeer' - type is not implemented event")
		if(typeof(listener) !== "function")
			throw new ReferenceError("Failed to execute 'addEventListener' on 'RTCPeer' - listener is not of type 'Function'")
		this.#listeners[type].push(listener);
	}

	removeEventListener(type, listener) {
		if(!this.#listeners[type])
			throw new ReferenceError("Failed to execute 'removeEventListener' on 'RTCPeer' - type is not implemented event")
		let index = this.#listeners[type].indexOf(listener);
		while(index !== -1) {
			this.#listeners[type].splice(index, 1);
			index = this.#listeners[type].indexOf(listener);
		}
	}

	get id() { return this.#id; }
	set id(v) { if(!this.#id) this.#id = v; }
	get state() { this.#channel?.readyState || "closed"; }
	get peers() { return this.#peers; }
}

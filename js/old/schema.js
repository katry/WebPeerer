class NetworkSchema {
	#network;
	#nodes;
	#queue;
	#locked;
	#ready;

	constructor(network) {
		this.#network = network;
		this.#nodes = {};
		this.#queue = [];
		this.#locked = false;
		this.#ready = false;
	}

	find(id) {
		let node = this.#nodes[id];
		let path = [];
		while(node?.parent) {
			path.unshift(node.id);
			node = node.parent;
		}
		return path;
	}

	check(one, two) {
		if(this.#locked) {
			this.#queue.push({action: "check", params: [one, two]});
			return;
		}
		this.lock();
		if(this.#nodes[one].parent?.id === two)
			this.updateNode(one);
		else if(this.#nodes[two].parent?.id === one)
			this.updateNode(two);
		this.unlock();
	}

	update(id) {
		if(this.#locked) {
			this.#queue.push({action: "update", params: [id]});
			return;
		}
		this.lock();
		if(this.#nodes[id])
			this.updateNode(id);
		else this.addNode(id);
		this.unlock();
	}

	restructure(id) {
		if(this.#locked) {
			this.#queue.push({action: "restructure", params: [id]});
			return;
		}
		this.lock();
		if(!this.#nodes[id])
			this.addNode(id);
		let queue = [id];
		let done = {};
		while(queue.length) {
			let node = this.#nodes[queue.shift()];
			if(done[node.id])
				continue;
			done[node.id] = true;
			let level = this.getLevel(node.id)+1;
			for(let key in this.#network.nodes[id].connectedNodes) {
				if(level < this.getLevel(key)) {
					this.#nodes[key].parent = node.id;
					queue.push(key);
				}
			}
		}
		this.unlock();
	}

	create() {
		this.#nodes = {};
		this.#nodes[this.#network.myNode.id] = this.createItem(this.#network.myNode.id, null);
		let queue = [this.#network.myNode.id];
		while(queue.length) {
			let node = queue.shift();
			for(let key in node.connectedNodes) {
				if(!this.#nodes[key]) {
					queue.push(key);
					this.#nodes[key] = this.createItem(key, node.id);
				}
			}
		}
		this.#ready = true;
	}

	load(treeArray) {
		this.#nodes[this.#network.myNode.id] = this.createItem(this.#network.myNode.id, null);
		for(let i in treeArray) {
			if(treeArray[i].id === this.#network.myNode.id)
				continue;
			let parent = treeArray[i].parent ||  this.#network.myNode.id;
			if(!this.#nodes[parent])
				this.#nodes[parent] = this.createItem(parent, null);
			if(!this.#nodes[treeArray[i].id])
				this.#nodes[treeArray[i].id] = this.createItem(treeArray[i].id, this.#nodes[parent]);
			else this.#nodes[treeArray[i].id].parent = this.#nodes[parent];
		}
		this.#ready = true;
	}

	save() {
		let array = [];
		for(let key in this.#nodes) {
			array.push({id: key, parent: this.#nodes[key].parent?.id || null});
		}
		return array;
	}

	lock() {
		this.#locked = true;
	}

	unlock() {
		this.#locked = false;
		while(this.#queue.length) {
			let operation = this.#queue.shift();
			switch(operation.action) {
				case "check":
					this.check(operation.params[0], operation.params[1]);
				break;
				case "update":
					this.update(operation.params[0]);
				break;
				case "restructure":
					this.restructure(operation.params[0]);
				break;
			}
		}
	}

	addNode(id) {
		if(this.#nodes[id])
			return;
		let minLevel = 0;
		let parent = null;
		for(let key in this.#network.nodes[id].connectedNodes) {
			let level = this.getLevel(key);
			if(!minLevel || minLevel > level) {
				minLevel = level;
				parent = this.#nodes[key];
			}
		}
		this.#nodes[id] = this.createItem(id, parent);
	}

	updateNode(id, exclude={}) {
		if(id === this.#network.myNode.id || exclude[id])
			return;
		exclude[id] = true;
		let node = this.#nodes[id];
		let previousLevel = this.getLevel(id) + 1;
		let minLevel = 0;
		let parent = null;
		for(let key in this.#network.nodes[id].connectedNodes) {
			if(!this.#nodes[key] || this.#nodes[key].parent === id) continue;
			let level = this.getLevel(key);
			if(!minLevel || minLevel > level) {
				minLevel = level;
				parent = this.#nodes[key];
			}
		}
		// console.log("parent", parent, this.#nodes);
		if(!parent || previousLevel > minLevel || !this.#nodes[this.#nodes[id].parent?.id]) {
			if(parent)
				node.parent = parent;
			else {
				delete this.#nodes[id];
				console.log("DELETED", id);
			}
			for(let key in this.#network.nodes[id].connectedNodes) {
				this.updateNode(key, exclude);
			}
		}
	}

	getLevel(id) {
		let node = this.#nodes[id];
		let level = 0;
		while(node?.parent) {
			level++;
			node = node.parent;
		}
		return level;
	}

	createItem(id, parent=null) {
		return {id: id, parent: parent};
	}

	get locked() {
		return this.#locked;
	}

	get nodes() {
		return this.#nodes;
	}

	get ready() {
		return this.#ready;
	}
}

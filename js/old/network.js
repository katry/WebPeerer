class Network extends NetworkSignaling {
	constructor(signal_servers=["ws://192.168.100.14:23574/"]) {
		super(localStorage.getItem('signals') || signal_servers);
		this.schema = new NetworkSchema(this);
		this.myNode = new NetworkNode(localStorage.getItem('node_id'), null, this);
		this.connected = false;
		this.nodes = {};
		let nodes = localStorage.getItem('nodes');
		for(let id in nodes) {
			this.nodes[id] = new Node(id, nodes[id], this);
			if(this.nodes[id].future_offer) {
				this.nodes[id].accept(this.nodes[id].future_offer, this.myNode.id);
			}
		}
		this.queue = [];
		this.connections = {};
		this.iceServers = [
			{ urls: 'stun:stun.nextcloud.com:443' },
			{ urls: 'stun:stun.ippi.fr:3478' },
			{ urls: 'stun:stun.hosteurope.de:3478' },
			{ urls: 'stun:stun.demos.ru:3478' },
			{ urls: 'stun:stun.l.google.com:19302' },
			{ urls: 'stun:stun1.l.google.com:19302' },
			{ urls: 'stun:stun2.l.google.com:19302' },
			{ urls: 'stun:stun3.l.google.com:19302' },
		];
		this.connect();
	}

	connect(forceWS) {
		this.connectSignal();
		if(Object.keys(this.nodes).length && !forceWS) {
		}
		else {
			this.firstNode = new NetworkNode(null, null, this);
			this.firstNode.connect();
		}
	}

	offer(offer, candidates, id) {
		if(this.connected) {
			this.send("offer", {
				"type": "offer",
				"sender": this.myNode.id,
				"target": id,
				"offer": offer,
				"candidates": candidates
			}, id);
		}
		else {
			let addr = Object.keys(this.signals)[0];
			this.sendSignal({
				"type": "offer",
				"sender": this.myNode.id,
				"offer": offer,
				"ws_addr": addr,
				"candidates": candidates
			}, addr);
		}
	}

	answer(answer, candidates, id, ws_addr=null, ws_id=null) {
		if(!ws_addr) {
			this.send("answer", {
				"type": "answer",
				"sender": this.myNode.id,
				"target": id,
				"answer": answer,
				"candidates": candidates
			}, id);
		}
		else {
			this.sendSignal({
				"type": "answer",
				"sender": this.myNode.id,
				"target": id,
				"ws_target": ws_id,
				"answer": answer,
				"candidates": candidates
			}, ws_addr);
		}
	}

	broadcast(action, data, excludes=[], sender=null) {
		excludes.push(this.myNode.id);
		for(let key in this.connections) {
			if(excludes.includes(key))
				continue;
			this.connections[key].send(action, data, null, sender, excludes);
		}
	}

	send(action, data, target, sender=null, path=[]) {
		if(target === this.myNode.id)
			return
		if(!path.length)
			path = this.schema.find(target);
		let next = path.shift()
		if(!this.nodes[next]) {
			this.schema.create();
			path = this.schema.find(target);
		}
		else if(!this.connections[next]) {
			this.schema.check(next);
			path = this.schema.find(target);
			console.log("next", next, this.schema.nodes[next], this.connections[next], action, data);
		}
		this.connections[next].send(action, data, target, sender, null, path);
	}

	onmessage(payload) {
		// console.log("\naction:", payload.action, "\nmy id:", this.myNode.id, "\ntarget:", payload.target, "\nsender:", payload.sender, "\ndata:", JSON.stringify(payload.data), "\n\n");
		if(payload.target && payload.target !== this.myNode.id) {
			this.send(payload.action, payload.data, payload.target, payload.sender, payload.path);
			return;
		}
		switch(payload.action) {
			case "state-connect":
				this.addConnection(payload);
			break;
			case "state-disconnect":
				console.log("disconnect...", payload);
				this.removeConnection(payload);
			break;
			case "offer":
				if(!this.nodes[payload.data.sender])
					this.nodes[payload.data.sender] = new NetworkNode(payload.data.sender, null, this);
				this.nodes[payload.data.sender].accept(payload.data);
			break;
			case "answer":
				this.nodes[payload.data.sender].confirm(payload.data, true);
			break;
		}
	}

	addConnection(payload) {
		for(let index in payload.data)
			if(!this.nodes[payload.data[index]])
				this.nodes[payload.data[index]] = new NetworkNode(payload.data[index], null, this);
		if(!this.nodes[payload.data[0]].connectedNodes[payload.data[1]] && ! this.nodes[payload.data[1]].connectedNodes[payload.data[0]])
		{
			this.nodes[payload.data[0]].connectedNodes[payload.data[1]] = this.nodes[payload.data[1]];
			this.nodes[payload.data[1]].connectedNodes[payload.data[0]] = this.nodes[payload.data[0]];
			this.broadcast("state-connect", payload.data, payload.excludes, payload.sender);
			this.schema.update(payload.data[1]);
		}
	}

	removeConnection(payload) {
		if(this.nodes[payload.data[0]] && this.nodes[payload.data[1]] && this.nodes[payload.data[0]].connectedNodes[payload.data[1]] && this.nodes[payload.data[1]].connectedNodes[payload.data[0]]) {
			delete this.nodes[payload.data[0]].connectedNodes[payload.data[1]];
			delete this.nodes[payload.data[1]].connectedNodes[payload.data[0]];
			console.log("removed connections", payload.data[0], payload.data[1]);
			this.schema.check(payload.data[0], payload.data[1]);
			if(!Object.keys(this.nodes[payload.data[0]].connectedNodes).length) {
				delete this.nodes[payload.data[0]];
				console.log("removed node", payload.data[0]);
			}
			if(!Object.keys(this.nodes[payload.data[1]].connectedNodes).length) {
				delete this.nodes[payload.data[1]];
				console.log("removed node", payload.data[1]);
			}
			this.broadcast("state-disconnect", payload.data, payload.excludes, payload.sender);
		}
	}
}

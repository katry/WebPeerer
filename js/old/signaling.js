class NetworkSignaling {
	constructor(servers) {
		this.signalServers = servers;
		this.signals = {};
		this.firstNode = null;
	}

	generateId() {
		let id;
		do {
			id = "";
			let random = Math.random().toString(36).substring(2)+Math.random().toString(36).substring(7);
			for(let i=0; i<random.length; i++)
				if(/^[a-zA-Z]+$/.test(random[i]) && Math.round(Math.random()))
					id += random[i].toUpperCase();
				else id += random[i];
		}
		while(this.nodes[id]);
		return id;
	}

	connectSignal(index=0) {
		try {
			let socket = new WebSocket(this.signalServers[index]);
			let queue = [];
			socket.addEventListener("message", (e) => {this.processSignal(e);});
			this.signals[this.signalServers[index]] = {ws: socket, queue: queue, id: null};
			window.addEventListener("beforeunload", () => {socket.close();});
		}
		catch(e) {
				delete this.signals[this.signalServers[index]];
				// TODO: show signal unavailable signal server error
		}
		if(++index < this.signalServers.length)
			this.connectSignal(index);
	}

	processSignal(e) {
		let data = null;
		try { data = JSON.parse(e.data); }
		catch(e) {
			console.log("error...", e);
			return;
		}
		switch(data.type) {
			case "offer":
				if(!data.sender || this.nodes[data.sender]?.connectedNodes.length)
					data.sender = this.generateId();
				delete this.nodes[data.sender];
				this.nodes[data.sender] = new NetworkNode(data.sender, null, this);
				this.nodes[data.sender].accept(data);
			break;
			case "answer":
				if(!this.nodes[data.sender]) {
					this.nodes[data.sender] = this.firstNode;
					this.firstNode = null;
				}
				this.nodes[data.sender].confirm(data, true);
			break;
			case "wsid":
				let socket = this.signals[e.target.url];
				socket.id = data.id;
				while(socket.queue.length)
					this.sendSignal(socket.queue.shift(), e.target.url)
			break;
			case "first":
				this.myNode = new NetworkNode(this.generateId(), null, this);
				this.schema.create();
				this.firstNode = null;
			break;
		}
	}

	sendSignal(data, addr) {
		if(this.signals[addr].id) {
			data["ws_sender"] = this.signals[addr].id;
			this.signals[addr].ws.send(JSON.stringify(data));
		}
		else {
			this.signals[addr].queue.push(data);
		}
	}
}

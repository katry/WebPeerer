class NetworkNode {
	#id;

	constructor(id, node, network) {
		this.#id = id || null;
		this.network = network;
		this.connectedNodes = {};
		this.connection = null;
		this.channel = null;
		this.stable = node?.stable || false;
		this.future_offer = node?.future_offer || null;
		this.iceCandidates = [];
		this.connected = false;
		this.firstNode = false;
	}

	async addCandidates(candidates) {
		for(let i in candidates) {
			await this.connection.addIceCandidate(new RTCIceCandidate(candidates[i]));
		}
	}

	connect() {
		this.connection = new RTCPeerConnection({'iceServers': this.network.iceServers});
		this.connection.addEventListener("iceconnectionstatechange", e => this.connectionstatechange(e));
		this.connection.addEventListener("icecandidate", (e) => {
			if (e.candidate) {
				this.iceCandidates.push(e.candidate);
			}
		});
		this.channel = this.connection.createDataChannel("channel");

		this.connection.addEventListener("icegatheringstatechange", () => {
			if(this.connection.iceGatheringState === "complete" && this.offer) {
				this.network.offer(this.offer, this.iceCandidates, this.#id);
			}
		});

		this.connection.createOffer().then(offer => {
			this.connection.setLocalDescription(offer).then(() => {
				if(this.connection.iceGatheringState === "complete")
					this.network.offer(offer, this.iceCandidates, this.#id);
				else this.offer = offer;
			});
		});

		this.channel.addEventListener("open", () => this.onconnection());
	}

	accept(data) {
		if(this.connectedNodes[this.network.myNode.id])
			return;
		this.connection = new RTCPeerConnection({'iceServers': this.network.iceServers});
		this.connection.addEventListener("iceconnectionstatechange", e => this.connectionstatechange(e));
		this.connection.addEventListener("icecandidate", (e) => {
			if (e.candidate) {
				this.iceCandidates.push(e.candidate);
			}
		});
		let prepared = false;

		this.connection.addEventListener("icegatheringstatechange", () => {
			if(this.connection.iceGatheringState == "complete") {
				if(this.answer)
					this.network.answer(this.answer, this.iceCandidates, data.sender, data.ws_addr, data.ws_sender);
				else prepared = true;
			}
		});

		this.connection.setRemoteDescription(new RTCSessionDescription(data.offer)).then(() => {
			this.connection.createAnswer().then(answer => {
				this.connection.setLocalDescription(answer).then(async () => {
					await this.addCandidates(data.candidates);
					if(prepared)
						this.network.answer(answer, this.iceCandidates, data.sender, data.ws_addr, data.ws_sender);
					else this.answer = answer;
				});
			});
		});

		this.connection.addEventListener("datachannel", (e) => {
			this.channel = e.channel;
			this.channel.addEventListener("open", () => this.onconnection());
		});
	}

	confirm(data, first=false) {
		if(first) {
			this.network.myNode = new NetworkNode(data.target, null, this.network);
			this.firstNode = true;
			this.#id = data.sender;
		}
		this.connection.setRemoteDescription(new RTCSessionDescription(data.answer)).then(() => {
			this.addCandidates(data.candidates);
		});
	}

	connectionstatechange() {
		if(!this.connection)
			return;
		console.log("state:", this.connection.iceConnectionState, this.#id);
		switch(this.connection.iceConnectionState) {
			case "connected":
			break;
			case "disconnected":
				if(this.network.queue.length)
					this.network.queue.shift().connect();
				this.disconnect();
			break;
			case "closed":
				this.disconnect(true);
			break;
		}
	}

	disconnect(recieved) {
		if(!this.connected)
			return;
		console.log("disconnected", this.#id);
		if(!recieved) {
			try {
				this.connection.close();
				this.channel.close();
			}
			catch(e) {}
		}
		delete this.connectedNodes[this.network.myNode.id];
		delete this.network.connections[this.#id];
		this.network.schema.check(this.network.myNode.id, this.#id);
		if(!Object.keys(this.connectedNodes).length) {
			delete this.network.nodes[this.#id];
		}
		this.connection = null;
		this.channel = null;
		this.connected = false;
		if(!Object.keys(this.network.connections).length)
			this.network.connected = false;
		this.network.broadcast("state-disconnect", [this.network.myNode.id, this.#id], [this.#id]);
	}

	send(action, data=null, target=null, sender=null, excludes=[], path=[]) {
		if(this.channel.readyState === "open")
			this.channel.send(JSON.stringify({
				action: action,
				data: data,
				excludes: excludes,
				sender: sender || this.network.myNode.id,
				target: target,
				path: path
			}));
	}

	onconnection() {
		this.channel.addEventListener("message", (e) => this.onmessage(e));
		this.channel.addEventListener("close", () => this.disconnect(true));
		this.channel.addEventListener("closing", () => this.disconnect(true));
		this.channel.addEventListener("error", () => this.disconnect());
		this.connection.addEventListener("close", () => this.disconnect(true));
		this.connectedNodes[this.network.myNode.id] = this.network.myNode;
		this.network.connections[this.#id] = this;
		this.network.myNode.connectedNodes[this.#id] = this
		this.connected = true;
		if(!this.network.connected) {
			this.network.connected = true;
			this.network.nodes[this.network.myNode.id] = this.network.myNode;
		}
		if(this.firstNode) {
			this.send("nodes-ask");
			this.firstNode = false;
		}
		else this.network.broadcast("state-connect", [this.network.myNode.id, this.#id], [this.#id]);

		if(this.network.schema.ready) {
			// this.network.schema.update(this.#id);
			this.network.schema.restructure(this.#id);
		}
		setTimeout(() => {
			if(this.network.queue.length)
				this.network.queue.shift().connect();
		}, 1000);
	}

	onmessage(e) {
		// console.log("node message", this.#id, e.data);
		let payload = null;
		try { payload = JSON.parse(e.data); }
		catch(e) { return; }
		switch(payload.action) {
			case "nodes-ask":
				this.sendNodes();
			break;
			case "nodes-init":
				this.initNodes(payload.data);
			break;
			default:
				this.network.onmessage(payload);
			break;
		}
	}

	sendNodes() {
		let nodes = {};
		let nodeList = Object.keys(this.network.nodes).filter(item => item!=this.#id);
		for(let item in nodeList)
			nodes[nodeList[item]] = Object.keys(this.network.nodes[nodeList[item]].connectedNodes);
		this.send("nodes-init", {nodes: nodes, schema: this.network.schema.save()});
	}

	initNodes(data) {
		for(let key in data.nodes) {
			if(!this.network.nodes[key]) {
				this.network.nodes[key] = new NetworkNode(key, null, this.network);
			}
			for(let i in data.nodes[key]) {
				let connection = data.nodes[key][i];
				if(!this.network.nodes[connection]) {
					this.network.nodes[connection] = new NetworkNode(connection, null, this.network);
					this.network.nodes[connection].connectedNodes[key] = this.network.nodes[key];
				}
				if(!this.network.nodes[key].connectedNodes[connection])
					this.network.nodes[key].connectedNodes[connection] = this.network.nodes[connection];
			}
		}
		this.network.schema.load(data.schema);
		let nodes = Object.keys(this.network.nodes).filter(item => {
			return item != this.network.myNode.id && !Object.keys(this.network.connections).includes(item);
		});
		let counter = 0;
		while(nodes.length && counter++ < 5) {
			let target = nodes.splice(Math.floor((Math.random()-1/1E9)*nodes.length), 1)[0];
			this.network.queue.push(this.network.nodes[target]);
		}
		if(this.network.queue.length)
			this.network.queue.shift().connect();
	}

	get id() {
		return this.#id;
	}
}
